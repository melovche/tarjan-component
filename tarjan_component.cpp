
/***
 * compile with 
 * g++ -I/nethome/yli3240/local/tools/boost_1_76_0 main.cpp -o lpreach
 */

#include <iostream>                  // for std::cout
#include <unordered_map>
#include <unordered_set>
#include <map>
#include <set>
#include <list>
#include <fstream>
#include <string>
#include <utility>                   // for std::pair
#include <algorithm>                 // for std::for_each
#include <boost/functional/hash.hpp> // for boost::hash
#include <boost/graph/graph_traits.hpp>
#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dijkstra_shortest_paths.hpp>


#include <boost/graph/adjacency_list.hpp>
#include <boost/graph/dominator_tree.hpp>
using namespace boost;
using namespace std;

class RegularExpression{
public:
    enum OPTYPE {SINGLETON, CONCAT, STAR, OR};
    // empty is 0, 0, 0
    // singleton oprand A is the eid
    // operator, inlucde 0 = noop (singleton), 1 concat, 2 star, 3 or
    int op;
    int oprandA;
    int oprandB;
    RegularExpression(int p, int rdA, int rdB){
        op = p;
        oprandA = rdA;
        oprandB = rdB;
    }
} ;
static std::unordered_map<string, int> regexids;
static int cur_regexid = 1;
static std::unordered_map<int, RegularExpression*> id2regex;
static std::unordered_map<RegularExpression*, int> regex2id;

class PathSeqItem {
public:
    RegularExpression* exp;
    int v;
    int w;
    PathSeqItem(RegularExpression* e, int vn, int wn){
        exp = e;
        v = vn;
        w = wn;
    }
} ;
class TarjanData{
public:
    // ancester is updated in initialize, mapping: vertex ordering ---> ancester ordering
    // if ancestor = -1 means no ancestor
    std::map<int, int> ancestor;
    // S is the path expression, mapping: vertex ordering ---> path expression
    std::map<int, RegularExpression*> S;
    // P is the path sequence P[edgestr][eid] is the replacement for edgeinfo.
    std::map<string, std::map<int, RegularExpression*>*> P;
    // sequence is the path sequence results
    std::vector<PathSeqItem*> sequence;
    TarjanData(){}
    void set_ancestor(int node_order, int ancestor_order){
        ancestor[node_order] = ancestor_order;
    }
    void set_S(int node_order, RegularExpression* pathexp){
        S[node_order] = pathexp;
    }

    
} ;

typedef adjacency_list<
    listS,
    listS,
    bidirectionalS,
    property<vertex_index_t, std::size_t>, no_property> G;
typedef pair<int, int> Edge;
typedef property_map<G, vertex_index_t>::type IndexMap;
typedef graph_traits<G>::vertex_descriptor Vertex;
typedef iterator_property_map<vector<Vertex>::iterator, IndexMap> PredMap;

class DomTreeNode {
public:
    std::list<DomTreeNode*> children;
    DomTreeNode* parent;
    int vtxidx;
    int ordering;
    DomTreeNode(int v){
        parent = NULL;
        vtxidx = v;
        ordering = -1;
    }

    void get_ordering(int& val){
        for(std::list<DomTreeNode*>::iterator it = children.begin(); it != children.end(); it++){
            (*it)->get_ordering(val);
        }
        this->ordering = val;
        val += 1;
        return;
    }

    void get_ordermap(std::unordered_map<int, int>& orderMap){
        orderMap[this->vtxidx] = this->ordering;
        for(std::list<DomTreeNode*>::iterator it = children.begin(); it != children.end(); it++){
            (*it)->get_ordermap(orderMap);
        }
        return;
    }

} ;
class DomTree{
public:
    DomTreeNode* root;
    DomTree(G& g, vector<int>& idom, std::unordered_map<int, string>& nid2str){
        IndexMap indexMap(get(vertex_index, g));
        graph_traits<G>::vertex_iterator uItr, uEnd;
        // count vertex number
        int vtxnum = 0;
        for (boost::tie(uItr, uEnd) = vertices(g); uItr != uEnd; ++uItr){
            vtxnum += 1;
        }
        // initialize treenode list
        DomTreeNode** treenode_list = new DomTreeNode*[vtxnum];
        for(int i = 0; i < vtxnum; i++){
            treenode_list[i] = new DomTreeNode(i);
        }
        // update treenode relationship
        for (boost::tie(uItr, uEnd) = vertices(g); uItr != uEnd; ++uItr){
            if(nid2str.find(get(indexMap, *uItr)) == nid2str.end()){
                // not in the original graph
                continue;
            }
            int nodeidx = indexMap[*uItr];
            int idomidx = idom[nodeidx];
            if(nid2str.find(idom[indexMap[*uItr]]) == nid2str.end()){
                cout << "Never happens.\n";
                continue;
            }
            if(nid2str.find(idomidx) == nid2str.end()){
                // no idom
                continue;
            }
            if(nid2str[idomidx] == "max"){
                this->root = treenode_list[nodeidx];
            }else{
                treenode_list[nodeidx]->parent = treenode_list[idomidx];
                treenode_list[idomidx]->children.push_back(treenode_list[nodeidx]);
            }
        }
        get_ordering(0);
    }

    void get_ordering(int low){
        int lval = low;
        this->root->get_ordering(lval);
    }

    std::unordered_map<int, int> get_ordermap(){
        std::unordered_map<int, int> orderMap;
        this->root->get_ordermap(orderMap);
        return orderMap;
    }

} ;
class EdgeInfo{
public:
    int src;
    int target;
    int edgeID;
    EdgeInfo(int s, int t, int id){
        src = s;
        target = t;
        edgeID = id;
    }
} ;
class LabelledGraph{
public:
    int vtxnum;
    std::unordered_map<std::string, int> str2eid;
    std::map<int, std::list<EdgeInfo*>*> in_edges;
    std::map<int, std::list<EdgeInfo*>*> out_edges;
    LabelledGraph(){vtxnum = -1;}
    /**
     * only interested in edges, can only be used to construct induced graphs.
     */
    void insert_edge(EdgeInfo* edgeinfo){
        int src = edgeinfo->src;
        int target = edgeinfo->target;
        if(in_edges.find(target) == in_edges.end()){
            in_edges[target] = new std::list<EdgeInfo*>();
        }
        in_edges[target]->push_back(edgeinfo);
        if(out_edges.find(src) == out_edges.end()){
            out_edges[src] = new std::list<EdgeInfo*>();
        }
        out_edges[src]->push_back(edgeinfo);
    }
    LabelledGraph(G& g, std::string& infile, std::unordered_map<string, int>& str2nid, std::unordered_map<int, int>& idx2order){
        ifstream inf(infile);
        string line;
        vtxnum = 0;
        int eid = 1;
        while(getline(inf, line)){
            string::size_type delimstart = line.find_first_of("[");
            string nodestr = line.substr(0, delimstart);
            string nodeA, nodeB;
            nodeA = nodestr.substr(0, nodestr.find_first_of("->")); 
            nodeB = nodestr.substr(nodestr.find_first_of("->")+2);
            int nodeA_order = idx2order[str2nid[nodeA]];
            int nodeB_order = idx2order[str2nid[nodeB]];
            string::size_type eidstart = line.find_first_of("=")+1;
            string::size_type eidend = line.find_first_of("]");
            string eidstr = line.substr(eidstart, eidend-eidstart);
            if(str2eid.find(eidstr) == str2eid.end()){
                str2eid[eidstr] = eid;
                eid++;
            }


            EdgeInfo* edgeinfo = new EdgeInfo(nodeA_order, nodeB_order, str2eid[eidstr]);
            vtxnum += 1;
            if(in_edges.find(nodeB_order) == in_edges.end()){
                in_edges[nodeB_order] = new std::list<EdgeInfo*>();
            }
            in_edges[nodeB_order]->push_back(edgeinfo);
            if(out_edges.find(nodeA_order) == out_edges.end()){
                out_edges[nodeA_order] = new std::list<EdgeInfo*>();
            }
            out_edges[nodeA_order]->push_back(edgeinfo);

        }
    }
} ;

RegularExpression* create_empty_regex(){
    std::string sig = "0_0_0";
    if(regexids.find(sig) != regexids.end()){
        return id2regex[regexids[sig]];
    }
    if(regexids.find(sig) == regexids.end()){
        regexids[sig] = 0;
    }
    RegularExpression* empty = new RegularExpression(0, 0, 0);
    id2regex[0] = empty;
    regex2id[empty] = 0;
    return empty;
}


void print_regex(int id){
    RegularExpression* regex = id2regex[id];
    if(regex->op == RegularExpression::SINGLETON){
        cout << regex->oprandA;
    }else if(regex->op == RegularExpression::STAR){
        cout << "( ";
        print_regex(regex->oprandA);
        cout << " )*";
    }else if(regex->op == RegularExpression::CONCAT){
        print_regex(regex->oprandA);
        cout << " ";
        print_regex(regex->oprandB);
    }else if(regex->op == RegularExpression::OR){
        cout << "( ";
        print_regex(regex->oprandA);
        cout << " ) || ( ";
        print_regex(regex->oprandB);
        cout << " )";
        
    }
}

void print_seq(PathSeqItem* pseq){
    print_regex(regex2id[pseq->exp]);
    cout << " , " << pseq->v;
    cout << " , " << pseq->w;
    cout << endl;
}
/**
 * if op == singleton, then idA is the edgeID, edgeID guaranteed > 0
 */
RegularExpression* create_regex(int op, int idA, int idB){
    if(op == 0 && idA == 0 && idB == 0){
        return create_empty_regex();
    }
    // empty cornor case:
    // SINGLETON ok
    // CONCAT
    if(op == RegularExpression::CONCAT){
        if(idA == 0){
            return id2regex[idB];
        }
        if(idB == 0){
            return id2regex[idA];
        }
    }else if(op == RegularExpression::STAR){
        if(idA == 0){
            return create_empty_regex();
        }
    }else if(op == RegularExpression::OR){
        if(idA == idB){
            return id2regex[idA];
        }
        if(idA == 0){
            return id2regex[idB];
        }
        if(idB == 0){
            return id2regex[idA];
        }
    }
    std::string sig = std::to_string(op);
    sig += "_";
    sig += std::to_string(idA);
    sig += "_";
    sig += std::to_string(idB);
    if(regexids.find(sig) == regexids.end()){
        regexids[sig] = cur_regexid;
        cur_regexid += 1;
    }else{
        return id2regex[regexids[sig]];
    }
    RegularExpression* res = new RegularExpression(op, idA, idB);
    id2regex[regexids[sig]] = res;
    regex2id[res] = regexids[sig];
    return res;
}

std::unordered_map<int, int> get_graph_ordering(G& g, vector<int>& idom, std::unordered_map<int, string>& nid2str){
    // currently print the original graph vertex id
    IndexMap indexMap(get(vertex_index, g));
    graph_traits<G>::edge_iterator eItr, eEnd;
    for(boost::tie(eItr, eEnd) = edges(g); eItr != eEnd; ++eItr){
        
        cout << indexMap[source(*eItr, g)] << "->" << indexMap[target(*eItr, g)] << endl;
    }

    DomTree* domtree = new DomTree(g, idom, nid2str);
    std::unordered_map<int, int> ordermap = domtree->get_ordermap();
    cout << "order map is: \n";
    for(std::unordered_map<int, int>::iterator it = ordermap.begin(); it != ordermap.end(); it++){
        cout << it->first << ": " << it->second << endl;
    }
    return ordermap;
    
}


vector<int> get_idom_map(G& g, std::unordered_map<int, string>& nid2str){
    vector<Vertex> domTreePredVector, domTreePredVector2;
    IndexMap indexMap(get(vertex_index, g));
    graph_traits<G>::vertex_iterator uItr, uEnd;
    int j = 0;
    for (boost::tie(uItr, uEnd) = vertices(g); uItr != uEnd; ++uItr, ++j)
    {
        //cout << "vtx count: " << j << endl;
      put(indexMap, *uItr, j);
    }

    domTreePredVector =
      vector<Vertex>(num_vertices(g), graph_traits<G>::null_vertex());
    PredMap domTreePredMap =
      make_iterator_property_map(domTreePredVector.begin(), indexMap);

    lengauer_tarjan_dominator_tree(g, vertex(0, g), domTreePredMap);
    
    vector<int> idom(num_vertices(g), -1);
    nid2str[(numeric_limits<int>::max)()] = "max";
    for (boost::tie(uItr, uEnd) = vertices(g); uItr != uEnd; ++uItr)
    {
      if (get(domTreePredMap, *uItr) != graph_traits<G>::null_vertex())
        idom[get(indexMap, *uItr)] =
          get(indexMap, get(domTreePredMap, *uItr));
      else
        idom[get(indexMap, *uItr)] = (numeric_limits<int>::max)();
      if(nid2str.find(get(indexMap, *uItr)) != nid2str.end() && nid2str.find(idom[get(indexMap,*uItr)]) != nid2str.end()){
      cout << nid2str[ get(indexMap, *uItr) ] << ": " << nid2str[ idom[get(indexMap, *uItr)] ] ;
      //cout << " originally is " << get(indexMap, *uItr) << ": " << idom[get(indexMap, *uItr)] ;
      cout << endl;
      }
    }

    //copy(idom.begin(), idom.end(), ostream_iterator<int>(cout, " "));
    cout << endl;
    return idom;
}

/***
 * return -1 if the idx is invalid
 * return the order if the idx is valid
 */
int get_node_order(int nodeidx, std::unordered_map<int, int>& idx2order){
    if(idx2order.find(nodeidx) == idx2order.end()){
        return -1;
    }
    return idx2order[nodeidx];
}

void mylink(int v, int w, TarjanData& data){
    data.ancestor[w] = v;
}

void update(int v, RegularExpression* regex, TarjanData& data){
    data.S[v] = regex;
}
void initialize(int nodeidx, std::unordered_map<int, int>& idx2order, TarjanData& data){
    int nodeorder = get_node_order(nodeidx, idx2order);
    if(nodeorder == -1){
        // nodeidx is invalid
        return;
    }else{
        data.set_ancestor(nodeorder, -1);
        data.set_S(nodeorder, create_empty_regex());
    }
}

Edge edgestr2edge(string edgestr){
    string nodeA, nodeB;
    nodeA = edgestr.substr(0, edgestr.find_first_of("_")); 
    nodeB = edgestr.substr(edgestr.find_first_of("_")+1);
    int nidA = std::stoi(nodeA);
    int nidB = std::stoi(nodeB);
    return make_pair(nidA, nidB);
    

}
std::string get_edgestr(Edge e){
    std::string res;
    res += std::to_string(e.first);
    res += "_";
    res += std::to_string(e.second);
    return res;
}

void compress_and_sequence(int u, TarjanData& data){
    if(data.ancestor[u] == -1)
        return;
    if(data.ancestor[data.ancestor[u]] != -1){
        PathSeqItem* pseq = new PathSeqItem(data.S[u], data.ancestor[u], u);
        data.sequence.push_back(pseq);
        data.S[u] = create_regex(RegularExpression::CONCAT, regex2id[data.S[data.ancestor[u]]], regex2id[data.S[u]]);
        data.ancestor[u] = data.ancestor[data.ancestor[u]];
    }
    return;
}

RegularExpression* eval_and_sequence(EdgeInfo* edge, TarjanData& data){
    compress_and_sequence(edge->src, data);
    RegularExpression* res = create_regex(RegularExpression::SINGLETON, edge->edgeID, 0);
    int curnode = edge->src;
    while(curnode != -1){
        PathSeqItem* pseq = new PathSeqItem(res, curnode, edge->target);
        data.sequence.push_back(pseq);
        res = create_regex(RegularExpression::CONCAT, regex2id[data.S[curnode]], regex2id[res]);
        curnode = data.ancestor[curnode];
    }

    return res; 
}

/***
 * get the induced subgraph for a vertex set S
 *
 */
LabelledGraph* get_induced_tilde_graph(LabelledGraph& graph, std::list<int>& S, std::unordered_map<string, Edge>& tilde_map){
    LabelledGraph* induced_graph = new LabelledGraph();
    std::unordered_set<int> S_vertices;
    for(std::list<int>::iterator it = S.begin(); it != S.end(); it++){
        S_vertices.insert(*it);
    }
    for(std::list<int>::iterator sit = S.begin(); sit != S.end(); sit++){
        int nid = *sit;
        if(graph.in_edges.find(nid) == graph.in_edges.end()){
            continue;
        }
        // tail of the edge is not changed by tilde_map
        for(std::list<EdgeInfo*>::iterator edgeit = graph.in_edges[nid]->begin(); edgeit != graph.in_edges[nid]->end(); edgeit++){
            EdgeInfo* edgeinfo = *edgeit;
            Edge e = make_pair(edgeinfo->src, edgeinfo->target);
            Edge tilde_e = tilde_map[get_edgestr(e)];
            EdgeInfo* tilde_edgeinfo = new EdgeInfo(tilde_e.first, tilde_e.second, edgeinfo->edgeID); 
            if(S_vertices.find(tilde_edgeinfo->src) != S_vertices.end()){
                induced_graph->insert_edge(tilde_edgeinfo);
            }
        }

    }
    return induced_graph;
}

std::list<PathSeqItem*>* eliminate_and_substitute(LabelledGraph& g,  std::map<string, std::map<int, RegularExpression*>*>& replace ){
    // signiture to regex id
    std::unordered_map<string, int> P;
    std::set<int> nodes;
    std::vector<int> vertices;
    for(map<int, std::list<EdgeInfo*>*>::iterator it = g.in_edges.begin(); it != g.in_edges.end(); it++){
        int nid = it->first;
        nodes.insert(nid);
    }
    for(map<int, std::list<EdgeInfo*>*>::iterator it = g.out_edges.begin(); it != g.out_edges.end(); it++){
        int nid = it->first;
        nodes.insert(nid);
    }
    for(std::set<int>::iterator it = nodes.begin(); it != nodes.end(); it++){
        vertices.push_back(*it);
    }
    
    // initialize
    for(int i = 0; i < vertices.size(); i++){
        int nid = vertices[i];
        if(g.out_edges.find(nid) == g.out_edges.end()){
            continue;
        }
        std::list<EdgeInfo*>* edgelst = g.out_edges[nid];
        for(std::list<EdgeInfo*>::iterator lstit = edgelst->begin(); lstit != edgelst->end(); lstit++){
            EdgeInfo* edgeinfo = *lstit;
            Edge e = make_pair(edgeinfo->src, edgeinfo->target);
            string edgestr = get_edgestr(e);
            if(P.find(edgestr) == P.end()){
                P[edgestr] = 0;
            }
            // replace tilde e
            RegularExpression* e_regex;
            if(replace.find(edgestr) != replace.end() && (*replace[edgestr]).find(edgeinfo->edgeID) != (*replace[edgestr]).end()){
                e_regex = (*replace[edgestr])[edgeinfo->edgeID];
            }else{
                e_regex = create_regex(RegularExpression::SINGLETON, edgeinfo->edgeID, 0);
            }
            int e_regexid = regex2id[e_regex];
            P[edgestr] = regex2id[create_regex(RegularExpression::OR, P[edgestr], e_regexid)];
        }
    }

    // loop
    for(int i = 0; i < vertices.size(); i++){
        int nid = vertices[i];
        Edge e = make_pair(nid, nid);
        string edgestr = get_edgestr(e);
        if(P.find(edgestr) == P.end()){
            P[edgestr] = 0;
        }
        P[edgestr] = regex2id[create_regex(RegularExpression::STAR, P[edgestr], 0)];

        for(int j = 0; j < vertices.size(); j++){
            int u = vertices[j];
            if(u <= nid){
                continue;
            }
            Edge e_uv = make_pair(u, nid);
            string uv_edgestr = get_edgestr(e_uv);
            if(P.find(uv_edgestr) == P.end()){
                continue;
            }
            P[uv_edgestr] = regex2id[create_regex(RegularExpression::CONCAT, P[uv_edgestr], P[edgestr])];

            for(int k = 0; k < vertices.size(); k++){
                int w = vertices[k];
                if(w <= nid){
                    continue;
                }
                Edge e_vw = make_pair(nid, w);
                string vw_estr = get_edgestr(e_vw);
                if(P.find(vw_estr) == P.end()){
                    continue;
                }
                Edge e_uw = make_pair(u, w);
                string uw_estr = get_edgestr(e_uw);
                if(P.find(uw_estr) == P.end()){
                    P[uw_estr] = 0;
                }
                int uvvw_regexid = regex2id[create_regex(RegularExpression::CONCAT, P[uv_edgestr],P[vw_estr])];
                P[uw_estr] = regex2id[create_regex(RegularExpression::OR, P[uw_estr], uvvw_regexid)];
            }
        }
    }
    std::map<int, std::list<PathSeqItem*>*> sort_pseq;
    for(std::unordered_map<string, int>::iterator pit = P.begin(); pit != P.end(); pit++){
        string edgestr = pit->first;
        Edge e = edgestr2edge(edgestr);
        if(e.first <= e.second && P[edgestr] != 0){
            if(sort_pseq.find(e.first) == sort_pseq.end()){
                sort_pseq[e.first] = new std::list<PathSeqItem*>();
            }
            PathSeqItem* pseqitem = new PathSeqItem(id2regex[P[edgestr]], e.first, e.second);
            sort_pseq[e.first]->push_back(pseqitem);
        }
    }
    
    std::list<PathSeqItem*>* res = new std::list<PathSeqItem*>();
    for(std::map<int, std::list<PathSeqItem*>*>::iterator it = sort_pseq.begin(); it != sort_pseq.end(); it++){
        int u = it->first;
        std::list<PathSeqItem*>* lst = it->second;
        for(std::list<PathSeqItem*>::iterator lstit = lst->begin(); lstit != lst->end(); lstit++){
            res->push_back(*lstit);
        }
    }

    return res;
}


/***
 *
 * children : a mapping from u_order ---> set of v_orders with idom[v_idx] = u_idx
 * non_tree : a mapping from u_order ---> set of edges e = (a_order, b_order) 
 *                                              with a_order != idom_order[u_order] and b_order = u_order 
 * tilde_map: a mapping from e_edgestr ---> tilde_e
 *
 */
void decompose_and_sequence(LabelledGraph& graph, G& g,  std::unordered_map<int, int>& idx2order, TarjanData& data, std::unordered_map<int, std::list<int>*>& children, std::unordered_map<int, std::list<EdgeInfo*>*>& non_tree, std::unordered_map<string, Edge>& tilde_map, std::unordered_map<int, std::list<EdgeInfo*>*>& tree){
    // initialize for each v in V do initialize(v) od,
    //            sequence = the empty sequence
    IndexMap indexMap(get(vertex_index, g));
    graph_traits<G>::vertex_iterator uItr, uEnd;
    for (boost::tie(uItr, uEnd) = vertices(g); uItr != uEnd; ++uItr){
        initialize(indexMap[*uItr], idx2order, data);
    }
    // [side note] sequence is automatically initialized into empty in TarjanData

    // loop:        for u := 1 until n do
    for(boost::tie(uItr, uEnd) = vertices(g); uItr != uEnd; ++uItr){
        int u_order = get_node_order(indexMap[*uItr], idx2order);
        if(u_order == -1){
            continue;
        }
        // derive:      for each v in children(u) do
        //                  for each e in nontree(v) do
        //                      P(~e) = EVAL_AND_SEQUENCE(e) od od;
        if(children.find(u_order) == children.end()){
            continue;
        }
        for(std::list<int>::iterator vit = children[u_order]->begin(); vit != children[u_order]->end(); vit++){
            int v = *vit;
            if(non_tree.find(v) == non_tree.end()){
                continue;
            }
            std::list<EdgeInfo*>* ntlist = non_tree[v];
            for(std::list<EdgeInfo*>::iterator ntit = ntlist->begin(); ntit != ntlist->end(); ntit++){
                EdgeInfo* edgeinfo = *ntit;
                Edge e = std::make_pair(edgeinfo->src, edgeinfo->target);
                Edge tilde_e = tilde_map[get_edgestr(e)]; 
                string tilde_edgestr = get_edgestr(tilde_e);
                EdgeInfo* tilde_edgeinfo = new EdgeInfo(tilde_e.first, tilde_e.second, edgeinfo->edgeID);
                if(data.P.find(tilde_edgestr) == data.P.end()){
                    data.P[tilde_edgestr] = new std::map<int, RegularExpression*>(); 
                }
                (*data.P[tilde_edgestr])[tilde_edgeinfo->edgeID] = eval_and_sequence(edgeinfo, data);
            }
        }
        // eliminate and substitute
        // compute a path sequence Xu for ~Gu
        LabelledGraph* tilde_Gu = get_induced_tilde_graph(graph, *(children[u_order]), tilde_map);
        std::list<PathSeqItem*>* pseqlist = eliminate_and_substitute(*tilde_Gu, data.P);
        for(std::list<PathSeqItem*>::iterator lstit = pseqlist->begin(); lstit != pseqlist->end(); lstit++){
            data.sequence.push_back(*lstit);
        }

        std::unordered_map<int, RegularExpression*> R;
        // solve
        for(std::list<int>::iterator vit = children[u_order]->begin(); vit != children[u_order]->end(); vit++){
            int v = *vit;
            R[v] = create_empty_regex();
            if(tree.find(v) == tree.end()){
                continue;
            }
            std::list<EdgeInfo*>* treelist = tree[v];
            for(std::list<EdgeInfo*>::iterator treeit = treelist->begin(); treeit != treelist->end(); treeit++){
                EdgeInfo* edgeinfo = *treeit;
                RegularExpression* e_regex = create_regex(RegularExpression::SINGLETON, edgeinfo->edgeID, 0);
                R[v] = create_regex(RegularExpression::OR, regex2id[R[v]], regex2id[e_regex]);
            }
        }

        for(std::list<PathSeqItem*>::iterator yit = pseqlist->begin(); yit != pseqlist->end(); yit++){
            PathSeqItem* pseq = *yit;
            int w = pseq->v;
            int x = pseq->w;
            if(R.find(w) == R.end()){
                R[w] = create_empty_regex();
            }
            if(w == x){
                R[w] = create_regex(RegularExpression::CONCAT, regex2id[R[w]], regex2id[pseq->exp]);
            }else{
                if(R.find(x) == R.end()){
                    R[x] = create_empty_regex();
                }
                RegularExpression* interm = create_regex(RegularExpression::CONCAT, regex2id[R[w]], regex2id[pseq->exp]);
                R[x] = create_regex(RegularExpression::OR, regex2id[R[x]], regex2id[interm]);
                
            }
        }

        // update
        for(std::list<int>::iterator vit = children[u_order]->begin(); vit != children[u_order]->end(); vit++){
            int v = *vit;
            update(v, R[v], data);
            mylink(u_order, v, data);
        }

        
    }

    //finalize
    RegularExpression* Q = create_empty_regex();
    // note that we assume the first node is the root
    int root = get_node_order(0, idx2order);
    if(root == -1){
    }
    if(non_tree.find(root) != non_tree.end()){
        std::list<EdgeInfo*>* ntlist = non_tree[root];

        for(std::list<EdgeInfo*>::iterator ntit = ntlist->begin(); ntit != ntlist->end(); ntit++){
            EdgeInfo* edgeinfo = *ntit;
            Q = create_regex(RegularExpression::OR, regex2id[Q], regex2id[eval_and_sequence(edgeinfo, data)]);
        }
    }
    if(regex2id[Q] != 0){
        PathSeqItem* pseq = new PathSeqItem(create_regex(RegularExpression::STAR, regex2id[Q], 0), root, root);
        data.sequence.push_back(pseq);
    }
    for(std::map<int, std::list<EdgeInfo*>*>::reverse_iterator vit = graph.in_edges.rbegin(); vit != graph.in_edges.rend(); vit++){
        int v = vit->first;
        PathSeqItem* pseq = new PathSeqItem(data.S[v], data.ancestor[v], v);
        data.sequence.push_back(pseq);
    }



}

/***
 * get the children set for vertices
 * children(u_order) = the set of v_orders such that idom(v_idx) = u_idx
 */
std::unordered_map<int, std::list<int>*> get_children(G& g, vector<int>& idom, std::unordered_map<int, string>& nid2str, std::unordered_map<int, int>& idx2order){
    std::unordered_map<int, std::list<int>*> children;
    IndexMap indexMap(get(vertex_index, g));
    graph_traits<G>::vertex_iterator uItr, uEnd;
    for (boost::tie(uItr, uEnd) = vertices(g); uItr != uEnd; ++uItr){
        if(nid2str.find(get(indexMap, *uItr)) == nid2str.end()){
            // not in the original graph
            continue;
        }
        //idom[nodeidx] = idomidx ; nodeidx --> v_idx, idomidx --> u_idx
        int nodeidx = indexMap[*uItr];
        int idomidx = idom[nodeidx];
        int nodeorder = get_node_order(nodeidx, idx2order);
        int idomorder = get_node_order(idomidx, idx2order);
        if(nodeorder == -1 || idomorder == -1){
            continue;
        }

        if(children.find(idomorder) == children.end()){
            children[idomorder] = new std::list<int>();
        }
        children[idomorder]->push_back(nodeorder);
    }
    return children;
}

void tarjan(LabelledGraph& graph, G& g,  std::unordered_map<int, int>& idx2order, std::unordered_map<int, std::list<int>*>& children, std::unordered_map<int, std::list<EdgeInfo*>*>& non_tree, std::unordered_map<std::string, Edge>& tilde_map, std::unordered_map<int, std::list<EdgeInfo*>*>& tree){
    std::map<int, RegularExpression*> pathexp;
    TarjanData data;
    decompose_and_sequence(graph, g, idx2order, data, children, non_tree, tilde_map, tree);    
}
std::unordered_map<int, std::list<EdgeInfo*>*> get_tree(LabelledGraph& graph, G& g, vector<int>& idom,std::unordered_map<int, string>& nid2str, std::unordered_map<int, int>& idx2order, std::unordered_map<int,int>& order2idx){
    std::unordered_map<int, std::list<EdgeInfo*>*> tree;
    for(std::map<int, std::list<EdgeInfo*>*>::iterator it = graph.out_edges.begin(); it != graph.out_edges.end(); it++){
        int src_order = it->first;
        std::list<EdgeInfo*>* outlist = it->second;
        for(std::list<EdgeInfo*>::iterator edgeit = outlist->begin(); edgeit != outlist->end(); edgeit++){
            EdgeInfo* edgeinfo = *edgeit;
            int target_order = edgeinfo->target;
            int srcidx = order2idx[src_order];
            int targetidx = order2idx[target_order];
            if(src_order == -1 || target_order == -1){
                std::cout << "Error: edge order not current for idx " << srcidx << " -> idx " << targetidx << endl; 
            }
            if(srcidx == idom[targetidx]){
                if(tree.find(target_order) == tree.end()){
                    tree[target_order] = new std::list<EdgeInfo*>();
                }
                tree[target_order]->push_back(edgeinfo); 
            }
        }
    }
    return tree;
}
/***
 * get the non_tree edge set for vertices
 * non_tree : a mapping from u_order ---> set of edges e = (a_order, b_order) 
 *                                              with a_order != idom_order[u_order] and b_order = u_order 
 * input idom : a mapping from idx to its idom idx
 * nid2str : node idx to string
 * idx2order : node idx to node order
 */
std::unordered_map<int, std::list<EdgeInfo*>*> get_non_tree(LabelledGraph& graph, G& g, vector<int>& idom, std::unordered_map<int, string>& nid2str, std::unordered_map<int, int>& idx2order, std::unordered_map<int, int>& order2idx){
    std::unordered_map<int, std::list<EdgeInfo*>*> non_tree;
    IndexMap indexMap(get(vertex_index, g));
    graph_traits<G>::edge_iterator eItr, eEnd;
    for(std::map<int, std::list<EdgeInfo*>*>::iterator it = graph.out_edges.begin(); it != graph.out_edges.end(); it++){
        int src_order = it->first;
        std::list<EdgeInfo*>* outlist = it->second;
        for(std::list<EdgeInfo*>::iterator edgeit = outlist->begin(); edgeit != outlist->end(); edgeit++){
            EdgeInfo* edgeinfo = *edgeit;
            int target_order = edgeinfo->target;
            int srcidx = order2idx[src_order];
            int targetidx = order2idx[target_order];
            if(src_order == -1 || target_order == -1){
                std::cout << "Error: edge order not current for idx " << srcidx << " -> idx " << targetidx << endl; 
            }
            // u_order <--> target_order <--> b_order;
            int b_idx = targetidx;
            int idom_u_idx = idom[b_idx];
            int idom_u_order = get_node_order(idom_u_idx, idx2order);
            if(src_order != idom_u_order){
                int u_order = target_order;
                if(non_tree.find(u_order) == non_tree.end()){
                    non_tree[u_order] = new std::list<EdgeInfo*>();
                }
                non_tree[u_order]->push_back(edgeinfo);
            }

        }
    }
    return non_tree;
}


/***
 * output: tilde_map : edgestr to its tilde edge
 * all nodes in order
 *
 */
std::unordered_map<std::string, Edge> get_tilde_map(LabelledGraph& graph, G& g, vector<int>& idom, std::unordered_map<int, string>& nid2str, std::unordered_map<int, int>& idx2order, std::unordered_map<int, int>& order2idx){
    std::unordered_map<std::string, Edge> tilde_map;
    IndexMap indexMap(get(vertex_index, g));
    graph_traits<G>::edge_iterator eItr, eEnd;
    for(std::map<int, std::list<EdgeInfo*>*>::iterator it = graph.out_edges.begin(); it != graph.out_edges.end(); it++){
        int src_order = it->first;
        std::list<EdgeInfo*>* outlist = it->second;
        for(std::list<EdgeInfo*>::iterator edgeit = outlist->begin(); edgeit != outlist->end(); edgeit++){
            EdgeInfo* edgeinfo = *edgeit;
            int target_order = edgeinfo->target;
            int srcidx = order2idx[src_order];
            int targetidx = order2idx[target_order]; 
            
            int res_src_order, res_target_order;
            res_target_order = get_node_order(targetidx, idx2order);

            int idomt_idx = idom[targetidx];
            if(srcidx == idomt_idx){
                res_src_order = get_node_order(srcidx, idx2order);    
            }else{
                int candidate_idx = srcidx;
                while(idom[candidate_idx] != idomt_idx){
                    candidate_idx = idom[candidate_idx];
                }
                res_src_order = get_node_order(candidate_idx, idx2order);
            }
            Edge e = std::make_pair(src_order, target_order);
            Edge tilde_e = std::make_pair(res_src_order, res_target_order);
            tilde_map[get_edgestr(e)] = tilde_e;
        }
    }
    return tilde_map;
}

void lp_reach(G& g, std::unordered_map<int, string>& nid2str, string& infile, std::unordered_map<string, int>& str2nid){
    // get idom map
    vector<int> idom = get_idom_map(g, nid2str);

    // get graph ordering
    std::unordered_map<int, int> idx2order = get_graph_ordering(g, idom, nid2str);
    std::unordered_map<int, int> order2idx;
    for(std::unordered_map<int, int>::iterator i2o_it = idx2order.begin(); i2o_it != idx2order.end(); i2o_it++){
        int idx = i2o_it->first;
        int order = i2o_it->second;
        order2idx[order] = idx;
    }
    ifstream inf(infile);
    string line;
    cout << "reordered graph is: \n";
    while(getline(inf, line)){
        string::size_type delimstart = line.find_first_of("[");
        string nodestr = line.substr(0, delimstart);
        string nodeA, nodeB;
        nodeA = nodestr.substr(0, nodestr.find_first_of("->")); 
        nodeB = nodestr.substr(nodestr.find_first_of("->")+2);
        cout << idx2order[str2nid[nodeA]] << "->" << idx2order[str2nid[nodeB]] << line.substr(delimstart) << endl;; 
    }
    inf.close();

    // tarjan's method
    LabelledGraph graph = LabelledGraph(g, infile, str2nid, idx2order);
    std::unordered_map<int, std::list<int>*> children = get_children(g, idom, nid2str, idx2order);
    std::unordered_map<int, std::list<EdgeInfo*>*> non_tree = get_non_tree(graph, g, idom, nid2str, idx2order, order2idx);
    std::unordered_map<std::string, Edge> tilde_map = get_tilde_map(graph, g, idom, nid2str, idx2order, order2idx);
    std::unordered_map<int, std::list<EdgeInfo*>*> tree = get_tree(graph, g, idom, nid2str, idx2order, order2idx);




    tarjan(graph, g, idx2order, children, non_tree, tilde_map, tree);
    // lpreach




    return;
}

typedef adjacency_list<vecS, vecS, bidirectionalS> Graph;


int main(int argc, char** argv){
    if(argc != 2){
        cout << "Usage: ./main <dotfile>\n";
        return 1;
    }

    string infile(argv[1]);
    cout << "Assuming the first node is the start node of the program.\n";
    cout << "processing graph " << infile << "\n";
    // declare a graph object
    std::unordered_set<Edge, boost::hash<Edge> > edgeset;

    ifstream inf(infile);
    string line;
    std::unordered_map<string, int> nodestr2nid;
    std::unordered_map<int, string> nid2str;
    
    int nid = 0;
    while(getline(inf, line)){
        string::size_type delimstart = line.find_first_of("[");
        string nodestr = line.substr(0, delimstart);
        //cout << nodestr << endl;
        string nodeA, nodeB;
        nodeA = nodestr.substr(0, nodestr.find_first_of("->")); 
        nodeB = nodestr.substr(nodestr.find_first_of("->")+2);
        //cout << nodeA << " to " << nodeB << endl;
        if(nodestr2nid.find(nodeA) == nodestr2nid.end()){
            nodestr2nid[nodeA] = nid;
            nid2str[nid] = nodeA;
            nid++;
        }
        if(nodestr2nid.find(nodeB) == nodestr2nid.end()){
            nodestr2nid[nodeB] = nid;
            nid2str[nid] = nodeB;
            nid++;
        }
        //cout << nodestr2nid[nodeA] << " to " << nodestr2nid[nodeB] << endl;
        edgeset.insert(make_pair(nodestr2nid[nodeA], nodestr2nid[nodeB]));
    }
    vector<Edge> edges;
    for(std::unordered_set<Edge, boost::hash<Edge> >::iterator it = edgeset.begin(); it != edgeset.end(); it++){
        edges.push_back(*it);
    }
    cout << "finish collecting edges.\n";



    G g(edges.begin(), edges.end(), edges.size());
    lp_reach(g, nid2str, infile, nodestr2nid);
    return 0;
}

